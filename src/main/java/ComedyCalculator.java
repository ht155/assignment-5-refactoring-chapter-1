public class ComedyCalculator extends PerformanceCalculator{
    public ComedyCalculator(Performance performance, Play play){
        super(performance, play);
    }

    public int getAmount() {
        int thisAmount = 30000;
        if (getPerformance().getAudience() > 20) {
            thisAmount += 10000 + 500 * (getPerformance().getAudience() - 20);
        }
        thisAmount += 300 * getPerformance().getAudience();
        return thisAmount;
    }

    @Override
    public int getVolumeCredits() {
        return super.getVolumeCredits() + (int)Math.floor((double) getPerformance().getAudience() / 5.0);
    }
}
