public class TragedyCalculator extends PerformanceCalculator{
    public TragedyCalculator(Performance performance, Play play){
        super(performance, play);
    }
    public int getAmount(){
        int thisAmount = 40000;
        if (getPerformance().getAudience() > 30) {
            thisAmount += 1000 * (getPerformance().getAudience() - 30);
        }
        return thisAmount;
    }
}
