import java.rmi.UnexpectedException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }
		this.customer = customer;
		this.performances = performances;
	}

	public String htmlStatement () {
		return renderHtml(createStatementData());
	}

	public String statement() {
		return renderPlainText(createStatementData());
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}

	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	private Play playFor(Performance aPerformance) {
		return plays.get(aPerformance.getPlayID());
	}

	private int amountFor(Performance perf){
		return new PerformanceCalculator(perf, playFor(perf)).getAmount();
	}
	private String usd(double aNmber){
		return new DecimalFormat("#.00").format((double) aNmber / 100.00);
	}

	private int totalVolumeCredits(StatementData data){
		return data.performances.stream().mapToInt(EnrichedPerformance::getVolumeCredits).sum();
	}

	private StatementData createStatementData() {

			StatementData st = new StatementData(this.customer,
					this.performances.stream().map(this::enrichPerformance).collect(Collectors.toList()));
			st.totalAmount = totalAmount(st);
			st.totalVolumeCredits = totalVolumeCredits(st);
			return st;


	}

	private PerformanceCalculator createPerformanceCalculator(Performance aPerformance, Play aPlay) throws UnexpectedException {
		switch (aPlay.getType()){
			case "tragedy": return new TragedyCalculator(aPerformance, aPlay);
			case "comedy": return new ComedyCalculator(aPerformance, aPlay);
			default:
				throw new UnexpectedException("Invalid Value" + aPlay.getType());
		}

	}
	private EnrichedPerformance enrichPerformance(Performance aPerformance) {
		try {
			PerformanceCalculator calculator = createPerformanceCalculator(aPerformance, playFor(aPerformance));
			EnrichedPerformance result = new EnrichedPerformance(aPerformance.getPlayID(), aPerformance.getAudience());
			result.setPlay(calculator.getPlay());
			result.setAmount(calculator.getAmount());
			result.setVolumeCredits(calculator.getVolumeCredits());
			return result;
		}
		catch (UnexpectedException e){
			System.out.println(e.getMessage());
		}
		return null;
	}

	private String renderPlainText(StatementData data){
		String result = "Statement for " + data.customer + "\n";
		for (EnrichedPerformance perf: data.performances) {
			if (playFor(perf) == null) {
				throw new IllegalArgumentException("No play found");
			}
			result += "  " + perf.getPlay().getName() + ": $" + usd(perf.getAmount()) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}
		result += "Amount owed is $" + usd(data.totalAmount) + "\n";
		result += "You earned " + 	data.totalVolumeCredits + " credits" + "\n";
		return result;
	}

	private String renderHtml (StatementData data) {
		String result = "<h1>Statement for ${" + data.customer + "}</h1>\n";
		result += "<table>\n";
		result += "<tr><th>play</th><th>seats</th><th>cost</th></tr>";
		for (EnrichedPerformance perf : data.performances) {
			result += "  <tr><td>${" + perf.getPlay().getName() + "}</td><td>${" + perf.getAudience() + "}</td>";
			result += "<td>${" + usd(perf.getAmount()) + "}</td></tr>\n";
		}
		result += "</table>\n";
		result += "<p>Amount owed is <em>${" + usd(data.totalAmount) + "}</em></p>\n";
		result += "<p>You earned <em>${"+ data.totalVolumeCredits +"}</em> credits</p>\n";
		return result;
	}


	private int volumeCredits(EnrichedPerformance perf) {
		int result = 0;
		result += Math.max(perf.getAudience() - 30, 0);
		if ( (perf.getPlay().getType().equals("comedy"))) {
			result += Math.floor((double) perf.getAudience() / 5.0);
		}
		return result;
	}

	private int totalAmount(StatementData data){
		return data.performances.stream().mapToInt(EnrichedPerformance::getAmount).sum();
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
