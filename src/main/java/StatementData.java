import java.util.ArrayList;
import java.util.List;

public class StatementData {
   public String customer;
   public List<EnrichedPerformance> performances;
    public int totalAmount;
    public int totalVolumeCredits;
   public StatementData(String customer, List<EnrichedPerformance> performances){
       this.customer = customer;
       this.performances = performances;
   }
}
