public class PerformanceCalculator {
    public Performance getPerformance() {
        return performance;
    }

    public void setPerformance(Performance performance) {
        this.performance = performance;
    }

    private Performance performance;

    public Play getPlay() {
        return play;
    }

    public void setPlay(Play play) {
        this.play = play;
    }

    private Play play;
    PerformanceCalculator(Performance performance, Play aPlay){
        this.performance = performance;
        this.play = aPlay;
    }

    public int getAmount(){
        /*int thisAmount = 0;
        switch (play.getType()) {
            case "tragedy": thisAmount = 40000;
                if (performance.getAudience() > 30) {
                    thisAmount += 1000 * (performance.getAudience() - 30);
                }
                break;
            case "comedy":  thisAmount = 30000;
                if (performance.getAudience() > 20) {
                    thisAmount += 10000 + 500 * (performance.getAudience() - 20);
                }
                thisAmount += 300 * performance.getAudience();
                break;
            default:
                throw new IllegalArgumentException("unknown type: " +  play.getType());
        }*/
        System.out.println("Subclass Responsibility");
        return -1;
    }

    public int getVolumeCredits(){
//        int result = 0;
//        result += Math.max(performance.getAudience() - 30, 0);
//        if ( (play.getType().equals("comedy"))) {
//            result += Math.floor((double) performance.getAudience() / 5.0);
//        }
        return  Math.max(performance.getAudience() - 30, 0);
    }

}
